import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  environmentName: string;

  constructor() {
    this.environmentName = environment.environmentName;
  }

  ngOnInit(): void {
    console.log(`using environment: ${this.environmentName}`);
  }

}
