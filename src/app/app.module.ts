import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './features/landing/landing.component';
import { TopNavigationComponent } from './core/top-navigation/top-navigation.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonsModule } from './shared/commons/commons.module';

@NgModule({
  declarations: [AppComponent, LandingComponent, TopNavigationComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CommonsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
