const url_config = {
    domain : "http://localhost:",
    port : "8080",
    context : "/api/v1",
    auth: "/auth"
  }
  
  export const environment = {
    production: false,
    environmentName: 'staging',
    url: `${url_config.domain}${url_config.port}${url_config.context}`,
    auth: `${url_config.domain}${url_config.port}${url_config.context}${url_config.auth}`
  };
  
  